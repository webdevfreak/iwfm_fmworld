<?php

/**
 * @file
 * iwfm_fmworld.views.inc
 */

/**
 * Implements hook_views_data().
 */
function iwfm_fmworld_views_data() {
  // Define the return array.
  $data = [];

  // The outermost keys of $data are Views table names, which should usually
  // be the same as the hook_schema() table names.
  $data['iwfm_fmworld'] = [];

  // The value corresponding to key 'table' gives properties of the table
  // itself.
  $data['iwfm_fmworld']['table'] = [];

  // Within 'table', the value of 'group' (translated string) is used as a
  // prefix in Views UI for this table's fields, filters, etc. When adding
  // a field, filter, etc. you can also filter by the group.
  $data['iwfm_fmworld']['table']['group'] = t('FM World Magazine');

  // Within 'table', the value of 'provider' is the module that provides schema
  // or the entity type that causes the table to exist. Setting this ensures
  // that views have the correct dependencies. This is automatically set to the
  // module that implements hook_views_data().
  $data['iwfm_fmworld']['table']['provider'] = 'iwfm_fmworld';

  // Some tables are "base" tables, meaning that they can be the base tables
  // for views. Non-base tables can only be brought in via relationships in
  // views based on other tables. To define a table to be a base table, add
  // key 'base' to the 'table' array:
  $data['iwfm_fmworld']['table']['base'] = [
    // Identifier (primary) field in this table for Views.
    'field' => 'id',
    // Label in the UI.
    'title' => t('FM World Magazine'),
    // Longer description in the UI. Required.
    'help' => t('FM World Magazine table.'),
    'weight' => -10,
  ];

  // Join.
  $data['iwfm_fmworld']['table']['join'] = [
    'file_managed' => [
      // Primary key field in file_managed to use in the join.
      'left_field' => 'fid',
      // Foreign key field in iwfm_fmworld to use in the join.
      'field' => 'magazine_image_id',
    ],
  ];

  // Plain text field, exposed as a field, sort, filter, and argument.
  $data['iwfm_fmworld']['id'] = [
    'title' => t('id'),
    'help' => t('FM World Magazine - ID field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'string',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'string',
    ],
  ];

  // Title field.
  $data['iwfm_fmworld']['title'] = [
    'title' => t('title'),
    'help' => t('FM World Magazine - Magazine Title field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'string',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'string',
    ],
  ];

  // issuu_code field.
  $data['iwfm_fmworld']['issuu_code'] = [
    'title' => t('issuu_code'),
    'help' => t('FM World Magazine - ISSUU Code field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'string',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'string',
    ],
  ];

  // magazine_image_id field.
  $data['iwfm_fmworld']['magazine_image_id'] = [
    'title' => t('id'),
    'help' => t('FM World Magazine - Image ID field.'),
    // - Use the implicit join method described above.
    'relationship' => [
      // Views name of the table to join to for the relationship.
      'base' => 'file_managed',
      // Database field name in the other table to join on.
      'base field' => 'fid',
      // ID of relationship handler plugin to use.
      'id' => 'standard',
      // Default label for relationship in the UI.
      'label' => t('File Managed fid field'),
    ],
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'string',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'string',
    ],
  ];

  // Created field.
  $data['iwfm_fmworld']['issue_date'] = [
    'title' => t('issue_date'),
    'help' => t('FM World Magazine - Issue date.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'string',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'string',
    ],
  ];

  // Created field.
  $data['iwfm_fmworld']['created'] = [
    'title' => t('created'),
    'help' => t('FM World Magazine - Created date field.'),
    'field' => [
      // ID of field handler plugin to use.
      'id' => 'standard',
    ],
    'sort' => [
      // ID of sort handler plugin to use.
      'id' => 'standard',
    ],
    'filter' => [
      // ID of filter handler plugin to use.
      'id' => 'string',
    ],
    'argument' => [
      // ID of argument handler plugin to use.
      'id' => 'string',
    ],
  ];

  return $data;
}
