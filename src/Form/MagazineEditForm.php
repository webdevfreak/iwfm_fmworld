<?php

namespace Drupal\iwfm_fmworld\Form;

use Drupal\file\Entity\File;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;

/**
 * Edit magazine data.
 */
class MagazineEditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fmworld_magazine_edit';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Record id.
    $edit_id = explode('/', \Drupal::service('path.current')->getPath());

    // Query database table to get this employment data.
    $result = db_select('iwfm_fmworld', 'fm')
      ->fields('fm')
      ->condition('fm.id', Xss::filter($edit_id[3]), '=')
      ->execute()
      ->fetchObject();

    // Define form fields.
    $form = [
      '#attributes' => ['enctype' => 'multipart/form-data'],
    ];

    $form['form_heading_1'] = [
      '#markup' => $this->t('<h2>FM World Magazine (Edit)</h2>'),
    ];

    $form['form_heading_2'] = [
      '#markup' => $this->t('<b>Form Details (* = mandatory fields):</b>'),
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Magazine Title'),
      '#default_value' => $result->title,
      '#required' => TRUE,
    ];

    $form['issuu_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('ISSUU URL'),
      '#default_value' => $result->issuu_code,
      '#required' => TRUE,
    ];

    $form['issue_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Issue Date'),
      '#default_value' => $result->issue_date,
      '#required' => TRUE,
    ];

    $validators = [
      'file_validate_extensions' => ['jpg png'],
      'file_validate_size' => [2048 * 2048],
    ];
    $form['magazine_image_id'] = [
      '#type' => 'managed_file',
      '#title' => t('Image File'),
      '#description' => $this->t('Maximum 2 MB file size allowed'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://fmworld_files/',
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Edit',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  /*public function validateForm(array &$form, FormStateInterface $form_state) {

  }*/

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Record id.
    $edit_id = explode('/', \Drupal::service('path.current')->getPath());

    // Query database table to get this employment data.
    $result = db_select('iwfm_fmworld', 'fm')
      ->fields('fm')
      ->condition('fm.id', Xss::filter($edit_id[3]), '=')
      ->execute()
      ->fetchObject();

    // Load old image file.
    $image_file_remove = File::load($result->magazine_image_id);

    // If image file object has data.
    if ($image_file_remove) {
      // Delete old image file.
      unlink($image_file_remove->getFileUri());

      // Delete image file record from file_managed table.
      file_delete($result->magazine_image_id);
    }

    // Update posted data in the database table.
    db_merge('iwfm_fmworld')
      ->key([
        'id' => Xss::filter($edit_id[3]),
      ])
      ->fields([
        'title' => Xss::filter($form_state->getValue('title')),
        'issuu_code' => Xss::filter($form_state->getValue('issuu_code')),
        'issue_date' => Xss::filter($form_state->getValue('issue_date')),
        'magazine_image_id' => Xss::filter($form_state->getValue('magazine_image_id')[0]),
        'created' => date('Y-m-d H:i:s'),
      ])
      ->execute();

    // Display success message.
    drupal_set_message($this->t('Magazine record successfully edited.'));

    // Redirect.
    $form_state->setRedirect('iwfm_fmworld.magazine_manage');
  }

}
