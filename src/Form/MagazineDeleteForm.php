<?php

namespace Drupal\iwfm_fmworld\Form;

use Drupal\file\Entity\File;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Url;

/**
 * Delete magazine data.
 */
class MagazineDeleteForm extends ConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fmworld_magazine_delete';
  }

  /**
   * {@inheritdoc}
   *
   * @param int $id
   *   (optional) The ID of the item to be deleted.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    return parent::buildForm($form, $form_state);  
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Record id.
    $edit_id = explode('/', \Drupal::service('path.current')->getPath());

    // Query database table to get this employment data.
    $result = db_select('iwfm_fmworld', 'fm')
      ->fields('fm')
      ->condition('fm.id', Xss::filter($edit_id[3]), '=')
      ->execute()
      ->fetchObject();

    // Load old image file.
    $image_file_remove = File::load($result->magazine_image_id);

    // If image file object has data.
    if ($image_file_remove) {
      // Delete old image file.
      unlink($image_file_remove->getFileUri());

      // Delete image file record from file_managed table.
      file_delete($result->magazine_image_id);
    }

    // Delete file from the database table.
    db_delete('iwfm_fmworld')->condition('id', $edit_id[3])->execute();

    // Display success message.
    drupal_set_message($this->t('Magazine record successfully deleted.'));

    // Redirect.
    $form_state->setRedirect('iwfm_fmworld.magazine_manage');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    // This needs to be a valid route otherwise the cancel link won't appear.
    return new Url('iwfm_fmworld.magazine_manage');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    // The question to display to the user.
    return $this->t('Do you really want to delete?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    // Record id.
    $edit_id = explode('/', \Drupal::service('path.current')->getPath());

    // Query database table to get this employment data.
    $result = db_select('iwfm_fmworld', 'fm')
      ->fields('fm')
      ->condition('fm.id', Xss::filter($edit_id[3]), '=')
      ->execute()
      ->fetchObject();

    // The question to display to the user.
    return $this->t('Do you really want to delete %id?', [
      '%id' =>
      $result->title,
    ]);
  }

}
