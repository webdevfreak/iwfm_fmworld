<?php

namespace Drupal\iwfm_fmworld\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Xss;

/**
 * Add magazine data.
 */
class MagazineAddForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fmworld_magazine_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Define form fields.
    $form = [
      '#attributes' => ['enctype' => 'multipart/form-data'],
    ];

    $form['form_heading_1'] = [
      '#markup' => $this->t('<h2>FM World Magazine (Add)</h2>'),
    ];

    $form['form_heading_2'] = [
      '#markup' => $this->t('<b>Form Details (* = mandatory fields):</b>'),
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Magazine Title'),
      '#required' => TRUE,
    ];

    $form['issuu_code'] = [
      '#type' => 'textarea',
      '#title' => $this->t('ISSUU URL'),
      '#required' => TRUE,
    ];

    $form['issue_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Issue Date'),
      '#default_value' => date('Y-m-d'),
    ];

    $validators = [
      'file_validate_extensions' => ['jpg png'],
      'file_validate_size' => [2048 * 2048],
    ];
    $form['magazine_image_id'] = [
      '#type' => 'managed_file',
      '#title' => t('Image File *'),
      '#description' => $this->t('Maximum 2 MB file size allowed'),
      '#upload_validators' => $validators,
      '#upload_location' => 'public://fmworld_files/',
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => 'Add',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  /*public function validateForm(array &$form, FormStateInterface $form_state) {

  }*/

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Save posted data in the database table.
    db_insert('iwfm_fmworld')
      ->fields([
        'title' => Xss::filter($form_state->getValue('title')),
        'issuu_code' => Xss::filter($form_state->getValue('issuu_code')),
        'issue_date' => Xss::filter($form_state->getValue('issue_date')),
        'magazine_image_id' => Xss::filter($form_state->getValue('magazine_image_id')[0]),
        'created' => date('Y-m-d H:i:s'),
      ])
      ->execute();

    // Display success message.
    drupal_set_message($this->t('Magazine record successfully added.'));

    // Redirect.
    $form_state->setRedirect('iwfm_fmworld.magazine_manage');
  }

}
